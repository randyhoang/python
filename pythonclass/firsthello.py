# -----------------------------------------------------------------------------
# Name:        firsthello
# Purpose:     our first Python program
#
# Author:      Randy Hoang
# -----------------------------------------------------------------------------
"""
A little python program that prints a greeting

Prompt the user for their name.
Print a customized Hello message
"""

print("Hello World")