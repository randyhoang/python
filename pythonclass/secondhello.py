# -----------------------------------------------------------------------------
# Name:        firsthello
# Purpose:     our first Python program
#
# Author:      Rula Khayrallah
# -----------------------------------------------------------------------------
name = input('Please enter your name: ') #Prompts the user for their name
print ("Hello", name) #Prints a personalized greeting for the variable name which was given from the input